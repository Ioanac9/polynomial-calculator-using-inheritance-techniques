package PT2018_30221_Cristea_Ioana_Assignment_1.tema;



import java.util.ArrayList;

import PT2018_30221_Cristea_Ioana_Assignment_1.tema.Monom;
import PT2018_30221_Cristea_Ioana_Assignment_1.tema.Polinom;
import junit.framework.TestCase;

public class testCalculatorPoly extends TestCase {

	public void testAdunare() {
		
	Polinom p1=new Polinom();Polinom p2=new Polinom();Polinom p3=new 

Polinom();

   	p1.getM().add(new Monom(3,12));
   	p2.getM().add(new Monom(4,18));
   
     p3= p1.add(p2);
 	assertEquals(12,p3.getM().get(0).getCoeficient() );
	}
	public void testInmultire() {
		
		Polinom p1=new Polinom();Polinom p2=new Polinom();Polinom 

p3=new Polinom();

	   	p1.getM().add(new Monom(3,12));
	   	p2.getM().add(new Monom(4,18));
	   
	     p3= p1.inmultit(p2);
	 	assertEquals(216,p3.getM().get(0).getCoeficient() );
	 	assertEquals(7,p3.getM().get(0).getPutere());
		}
public void testScadere() {
		
		Polinom p1=new Polinom();Polinom p2=new Polinom();Polinom 

p3=new Polinom();

	   	p1.getM().add(new Monom(3,12));
	   	p2.getM().add(new Monom(3,18));
	   
	     p3= p1.sub(p2);
	 	assertEquals(-6,p3.getM().get(0).getCoeficient() );
	 	assertEquals(3,p3.getM().get(0).getPutere());
		}
public void testDerivare() {
	
	Polinom p1=new Polinom();Polinom p2=new Polinom();

   	p1.getM().add(new Monom(3,12));
   
     p2=p1.deriveaza();
 	assertEquals(36,p2.getM().get(0).getCoeficient() );
 	assertEquals(2,p2.getM().get(0).getPutere());
	}

}
