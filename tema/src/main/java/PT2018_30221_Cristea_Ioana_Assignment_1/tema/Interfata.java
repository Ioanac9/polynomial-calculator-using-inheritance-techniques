package PT2018_30221_Cristea_Ioana_Assignment_1.tema;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Interfata extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel pane = new JPanel(new GridBagLayout());
	GridBagConstraints c = new GridBagConstraints();
	private JButton buttonIn1 = new JButton("OK");
	private JButton buttonIn2 = new JButton("OK");
	private JButton buttonAdd = new JButton("+");
	private JButton buttonSub = new JButton("-");
	private JButton buttonInm = new JButton("*");
	private JButton buttonDiv = new JButton("/");
	private JButton buttonDeriv1 = new JButton("Derivare");
	private JButton buttonDeriv2= new JButton("Derivare");
	private JButton buttonInteg1 = new JButton("Integrare");
	private JButton buttonInteg2 = new JButton("Integrare");
	
	
	private JTextField text1 = new JTextField(20);
	private JTextField text2 = new JTextField(20);
	private JTextField textResult1= new JTextField(20);
	private JTextField textResult2= new JTextField(20);
	private JTextField textResultadd= new JTextField(20);
	private JTextField textResultsub= new JTextField(20);
	private JTextField textResultinm= new JTextField(20);
	private JTextField textResultimp= new JTextField(20);
	private JTextField textResultderiv1= new JTextField(20);
	private JTextField textResultderiv2= new JTextField(20);
	private JTextField textResultinteg1= new JTextField(20);
	private JTextField textResultinteg2= new JTextField(20);
	
	private JLabel label1 = new JLabel("Polinom1");
	private JLabel label2= new JLabel("Polinom2");
	private JLabel labelR1= new JLabel("Afisare polinom1:");
	private JLabel labelR2= new JLabel("Afisare polinom2:");
	private JLabel labelRadd= new JLabel("Rezultat adunare:");
	private JLabel labelRsub= new JLabel("Rezultat scadere:");
	private JLabel labelRinm= new JLabel("Rezultat inmultire:");
	private JLabel labelRimp= new JLabel("Rezultat impartire:");
	private JLabel labelRderiv1= new JLabel("Rezultat derivare1:");
	private JLabel labelRderiv2= new JLabel("Rezultat derivare2:");
	private JLabel labelRinteg1= new JLabel("Rezultat integrare1:");
	private JLabel labelRinteg2= new JLabel("Rezultat integrare2:");

	public Interfata(String name) {
		super(name);
		//polinom1
		c.gridx = 0;	c.gridy = 0;
		pane.add(label1,c);
		c.gridx = 1;c.gridy = 0;
		pane.add(text1, c);
		c.gridx = 2;c.gridy = 0;	
		pane.add(buttonIn1, c);
		c.gridx = 2;c.gridy = 2;	
		pane.add(buttonDeriv1,c);
		c.gridx = 3;c.gridy = 2;	
		pane.add(buttonInteg1,c);
		c.gridx = 0;c.gridy = 2;
		pane.add(labelR1, c);
		c.gridx = 1;c.gridy = 2;
		pane.add(textResult1,c);		
       //polinom2	
		c.gridx = 0;c.gridy = 1;
		pane.add(label2,c);
		c.gridx = 1;	c.gridy = 1;
		pane.add(text2, c);
		c.gridx = 2;c.gridy = 1;	
		pane.add(buttonIn2, c);
		c.gridx = 2;c.gridy = 3;	
		pane.add(buttonDeriv2,c);
		c.gridx = 3;c.gridy = 3;	
		pane.add(buttonInteg2,c);
		c.gridx = 0;c.gridy = 3;
		pane.add(labelR2, c);
		c.gridx = 1;c.gridy = 3;
		pane.add(textResult2, c);
		//rezultate_calcule
		c.gridx = 0;c.gridy = 4;	
		pane.add(buttonAdd, c);
		c.gridx = 1;c.gridy = 4;	
		pane.add(buttonSub, c);
		c.gridx = 2;c.gridy = 4;	
		pane.add(buttonInm, c);
		c.gridx = 3;c.gridy = 4;	
		pane.add(buttonDiv, c);
		c.gridx = 0;c.gridy = 6;	
		pane.add(labelRadd, c);
		c.gridx = 1;c.gridy = 6;	
		pane.add(textResultadd, c);
		c.gridx = 0;c.gridy = 7;	
		pane.add(labelRsub, c);
		c.gridx = 1;c.gridy = 7;	
		pane.add(textResultsub, c);
		c.gridx = 0;	c.gridy = 8;	
		pane.add(labelRinm, c);
		c.gridx = 1;c.gridy = 8;	
		pane.add(textResultinm, c);
		c.gridx = 0;c.gridy = 9;	
		pane.add(labelRimp, c);
		c.gridx = 1;c.gridy = 9;	
		pane.add(textResultimp, c);
		c.gridx = 0;c.gridy = 10;	
		pane.add(labelRderiv1, c);
		c.gridx = 1;c.gridy = 10;	
		pane.add(textResultderiv1, c);
		c.gridx = 0;c.gridy = 11;	
		pane.add(labelRderiv2, c);
		c.gridx = 1;c.gridy = 11;	
		pane.add(textResultderiv2, c);
		c.gridx = 0;c.gridy = 12;	
		pane.add(labelRinteg1, c);
		c.gridx = 1;c.gridy = 12;	
		pane.add(textResultinteg1, c);
		c.gridx = 0;c.gridy = 13;	
		pane.add(labelRinteg2, c);
		c.gridx = 1;c.gridy = 13;	
		pane.add(textResultinteg2, c);
		
		
		buttonIn1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {		

				Polinom polinom1=new Polinom();
				Polinom polinom2=new Polinom();
				String s = text1.getText();			
				try {			
					polinom1.initializare(s);
				    polinom1.afisareContinut();
					textResult1.setText(polinom1.getSirP());		
				}catch(NumberFormatException e) { 
					textResult1.setText("Formatul nu este valid");
				}
			}		
		});

		buttonIn2.addActionListener(new ActionListener(){	    
			public void actionPerformed(ActionEvent arg0) {
				String s = text2.getText();

				Polinom polinom1=new Polinom();
				Polinom polinom2=new Polinom();
				try {
				    polinom2.initializare(s);
					polinom2.afisareContinut();
					textResult2.setText(polinom2.getSirP());			
			    }catch(NumberFormatException e) {  		
			    	textResult2.setText("Formatul nu este valid");
			    }
			}
		});
		
		buttonAdd.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent arg0) {	
			   Polinom polinomRez=new Polinom();			
			   String s2 = text2.getText();
			   String s1 = text1.getText();

				Polinom polinom1=new Polinom();
				Polinom polinom2=new Polinom();
			   try {
					polinom1.initializare(s1);	
					polinom2.initializare(s2);		
					//System.out.println(polinom1.getSirP());
					polinomRez = polinom1.add(polinom2);
					//System.out.println(polinomRez.getSirP());
					polinomRez.afisareContinut();
					//System.out.println(polinomRez.getSirP());
					textResultadd.setText(polinomRez.getSirP());	
			   }catch(Exception e) {	
				    textResultadd.setText("Formatul nu este valid");					
				}
		    }
		});
		buttonSub.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent arg0) {
			Polinom polinomRez=new Polinom();	
			String s2 = text2.getText();
			String s1 = text1.getText();

			Polinom polinom1=new Polinom();
			Polinom polinom2=new Polinom();
			try {
			polinom1.initializare(s1);	
			polinom2.initializare(s2);		
            polinomRez = polinom1.sub(polinom2);
            polinomRez.afisareContinut();
            textResultsub.setText(polinomRez.getSirP());
			}catch(Exception e) {
				textResultsub.setText("Formatul nu este valid");	
			}
		}
	});
		buttonInm.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent arg0) {
		    	Polinom polinomRez=new Polinom();	
		    	String s2 = text2.getText();
		    	String s1 = text1.getText();

				Polinom polinom1=new Polinom();
				Polinom polinom2=new Polinom();
		    	try {
		    		polinom1.initializare(s1);	
			    	polinom2.initializare(s2);		
			    	polinomRez = polinom1.inmultit(polinom2);
			    	polinomRez.afisareContinut();
			    	textResultinm.setText(polinomRez.getSirP());		
		    	}catch(Exception e) {
					textResultinm.setText("Formatul nu este valid");	
				}
		    }
		});
			
		buttonDeriv1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				Polinom polinomRez=new Polinom();
				String s1 = text1.getText();	

				Polinom polinom1=new Polinom();
				Polinom polinom2=new Polinom();
				try {
					polinom1.initializare(s1);	
					polinomRez=polinom1.deriveaza();       
					polinomRez.afisareContinut();
					textResultderiv1.setText(polinomRez.getSirP());
				}catch(Exception e) {
					textResultderiv1.setText("Formatul nu este valid");	
				}
			}
		});
				
		buttonDeriv2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				Polinom polinomRez=new Polinom();
				String s2 = text2.getText();	

				Polinom polinom1=new Polinom();
				Polinom polinom2=new Polinom();
				try {
					polinom2.initializare(s2);	
					polinomRez=polinom2.deriveaza();
					polinomRez.afisareContinut();
					textResultderiv2.setText(polinomRez.getSirP());				
				}catch(Exception e) {
					textResultderiv2.setText("Formatul nu este valid");	
				}
			}
		});	

		buttonInteg1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				Polinom polinomRez=new Polinom();
				String s1 = text1.getText();

				Polinom polinom1=new Polinom();
				Polinom polinom2=new Polinom();
				try {
					polinom1.initializare(s1);	
					polinomRez=polinom1.integreaza();
					polinomRez.afisareContinut();
					textResultinteg1.setText(polinomRez.getSirP());
				}catch(Exception e) {
					textResultinteg1.setText("Formatul nu este valid");	
				}
			}
		});

		buttonInteg2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				Polinom polinomRez=new Polinom();
				String s2 = text2.getText();	

				Polinom polinom1=new Polinom();
				Polinom polinom2=new Polinom();
				try {
					polinom2.initializare(s2);	
					polinomRez=polinom2.integreaza();
					polinomRez.afisareContinut();
					textResultinteg2.setText(polinomRez.getSirP());				
				}catch(Exception e) {
					textResultinteg2.setText("Formatul nu este valid");	
				}
			}
});					
		this.add(pane);
	}
     
	public static void main(String args[]) {
		JFrame frame = new Interfata("Calculator de polinoame");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
	}
}