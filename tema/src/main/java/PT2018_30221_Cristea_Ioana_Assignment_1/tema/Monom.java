package PT2018_30221_Cristea_Ioana_Assignment_1.tema;


import java.lang.*;
import java.util.ArrayList;
import java.util.Comparator;



public class Monom {
    private int putere;
	private int coeficient;
	private String sir;

public Monom(String s) {
	this.setSir(s);
	}
public Monom(String c,String p) {
		this.putere=Integer.parseInt(p);
		this.setCoeficient(Integer.parseInt(c));
	}
public Monom(int putere, int coeficient) {
		this.putere=putere;
		this.setCoeficient(coeficient);
	}
 public Monom inmultireMon(Monom monom2) {
		Monom rezMon;
		int newCoef = this.getCoeficient() * monom2.getCoeficient();
		int newPow= this.getPutere() + monom2.getPutere();
		rezMon = new Monom(newPow,newCoef);
		return rezMon;
	}
public void adunaCoeficient(int valoareCoeficient) {
		this.setCoeficient(this.getCoeficient() + valoareCoeficient);	
	}
public static Comparator<Monom> ComparaDupaPutere() {   
		Comparator<Monom> compMon = new Comparator<Monom>(){
		        public int compare(Monom s1, Monom s2) {
		    	return Integer.compare(s1.putere, s2.putere);
		        }	        
		};
		return compMon;
	}  
public void separareMonoame(String s) {
		StringBuilder str = new StringBuilder(s);		
		str.insert(0, '+');
		 s=str.toString();	
		for (int i = 2; i < s.length(); i++) {
			if (s.charAt(i)=='+'||s.charAt(i)=='-') {
				 str.insert(i, '+');
				 i++;
				 s=str.toString();
			}
		}
		 str.insert(str.length(), '+');
		 s=str.toString();
		this.setSir(s);
	}
public void delimitareCoeficientPutere(String msir,Monom m) {
   
		int ok=0;
						for(int j=0;j<msir.length();j++) {
							
							if(msir.charAt(j)=='^') { 
								this.setCoeficient(Integer.parseInt(msir.substring(0, j)));
								this.putere=Integer.parseInt(msir.substring(j+1,msir.length()));
								ok=1;
							}
								else 
									if(j==msir.length()-1 && ok==0) {
							   this.setCoeficient(Integer.parseInt(msir));
							   this.putere=0;
							}
						}
	}
public void derivareMonom() {
		if (putere != 0) {
			setCoeficient(getCoeficient() * putere);
			putere=putere-1;
		}else	
			setCoeficient(0);
	}
public void integrareMonom() {
		putere=putere+1;
		setCoeficient(getCoeficient()/putere);	
	}
public int getCoeficient() {
		return coeficient;
	}
public void setCoeficient(int coeficient) {
		this.coeficient = coeficient;
	}		
public int getPutere() {
		return this.putere;
	}
public void setPutere(int putere) {
		this.putere = putere;
	}
public String getSir() {
		return sir;
	}
public void setSir(String sir) {
		this.sir = sir;
	}

}