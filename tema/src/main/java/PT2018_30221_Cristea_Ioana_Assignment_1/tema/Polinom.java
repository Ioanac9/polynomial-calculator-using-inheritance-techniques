package PT2018_30221_Cristea_Ioana_Assignment_1.tema;

import java.util.*;
public class Polinom {
private ArrayList<Monom> m;
private String sirP=null;
 
 public Polinom() {                     //constructorul nul 
	 setM(new ArrayList<Monom>());
 }
public Polinom(ArrayList<Monom> m2) {   //constructorul care initializa doar arrayList-ul de monoame
    setM(m2);
}
public Polinom(Monom m3) { //constructorul care initializeaza  arrayList-ul de monoame un singur element
		m.add(m3);
}
public void golirePolinom() {
	for (int k=0;k<this.getM().size();k++) { //se goleste continutul listei de monoame
		this.getM().clear();
	}
}
public void initializare(String s) {  //se initializeaza polinomul cu valori nule
	this.golirePolinom();		
	Monom monom=new Monom(s);
	monom.separareMonoame( s);	//se separa monoamele prin adaugarea semnului "+" intre ele
	this.crearePolinom(monom.getSir()) ;//noul sir va fi transmis spre construirea polinomului
 }
public void afisareContinut() {
	this.setSirP(new String());
		for (Monom monomCurent : this.getM()) {
			if(monomCurent.getCoeficient()<0)  //afisarea continutului listei de monoame 
			this.setSirP(this.getSirP()+monomCurent.getCoeficient()+"X^"+monomCurent.getPutere());
			else //se memoreaza in sirP din polinomul curent formatul specific de afisat 
			this.setSirP(this.getSirP()+"+"+monomCurent.getCoeficient()+"X^"+monomCurent.getPutere());
		}
}
public void crearePolinom(String s)  {
	 String msir = null;
	 Monom monom=new Monom(s);
			for (int i = 0; i < s.length(); i++) { 
				if (s.charAt(i)=='+') {//se parcurge sirul nou modificat si se extrag subsirurile curpinse intre "+ si +"
					for(int k=i+2;k<s.length();k++) {
						if(s.charAt(k)=='+') {								
							 msir=s.substring(i+1, k); //aceste subsiruri vor reprezenta monoamele
							 i=i+1;
						    monom.delimitareCoeficientPutere(msir,monom);//delimitarea monomului curent extras se va face apeland aceasta functie 
						    this.getM().add(new Monom(monom.getPutere(),monom.getCoeficient())); 			//se adauga in polinomul de construit monoamele formate		    
							break;						    							
						}
					}				
			}		
		}
			this.analizaSoartarePolinom(); //se apeleaza sortarea si analiza noului polinom 
 }
private void analizaSoartarePolinom() {
	int putere1=0,putere2=0;
	for(int i=0;i<getM().size();i++) {
		putere1 = getM().get(i).getPutere(); //se parcurge lista de monoame comparandu-se fiecare element unul cu altul si se verifica 
		 for(int j=i+1;j<getM().size();j++) {  //daca se gasesc dubluri de putere ,in cazul in care se gasesc ,se aduna monoamele la care 
		    putere2 = getM().get(j).getPutere(); //s-au gasit duplicatele 
			if(putere1 == putere2) {
				getM().get(i).adunaCoeficient(getM().get(j).getCoeficient());
				this.getM().remove(j);
			}
		}
	}
		Collections.sort(this.getM(), Monom.ComparaDupaPutere()); //se sorteaza lsiat de monoame dupa putere
}
public Polinom add(Polinom polinom2) {		  
	  Polinom polinomRez = new Polinom();
	  polinomRez.setSirP(new String());
		for(int i=0;i<this.getM().size();i++) {// polinoamele sunt sortate
			int putere1 = this.getM().get(i).getPutere();
			int coeficient1 = this.getM().get(i).getCoeficient();			
			int indexPutere= polinom2.getIndexPutere(putere1);
			if(indexPutere== -2) {//daca in polinomul 2 nu se gaseste puterea din polinomul 1 atunci se adauga in  polinomul 3 doar puterea din primul polinom 
				polinomRez.getM().add(new Monom(putere1,coeficient1));
			} else {//daca s-a gasit in polinomul 2 puterea din polinomul 1 
				int putere2 = polinom2.getM().get(indexPutere).getPutere();
				int coeficient2 = polinom2.getM().get(indexPutere).getCoeficient();
				polinomRez.getM().add(new Monom( putere2,coeficient1+coeficient2));
				polinom2.getM().remove(indexPutere); //eliminam puterea tocmai adaugata din polinomul 2
			}
			polinomRez.removeCoefZero();//ne asiguram ca eliminam coeficientii cu 0
		}	
		for(int j=0;j<polinom2.getM().size();j++) {    // dupa eventuala "eliminare" a monoamelor din polinomul 2 ,se mai parcurge 1 data si acest polinom
			int p2 = polinom2.getM().get(j).getPutere(); //pentru a adauga valorile corespunzatoare in polinomul rezultat
			int c2 = polinom2.getM().get(j).getCoeficient();
			polinomRez.getM().add(new Monom(p2,c2));
			polinomRez.removeCoefZero();
		}
	  polinomRez.analizaSoartarePolinom(); //reorganizam noul polinom rezultat
		return polinomRez;	  
	}  
public Polinom sub(Polinom polinom2) {
		Polinom polinomRez;
		for(Monom monomCurent:polinom2.getM()) {//ne-am folosit de adunare pentru a implementa scaderea
			monomCurent.setCoeficient(monomCurent.getCoeficient()*(-1)); //setam al doilea operand cu semnul -
		}  
		polinomRez = this.add(polinom2);
		return polinomRez;
	}
public Polinom inmultit(Polinom polinom2) {
	Polinom polinom3 = new Polinom();
	polinom3.setSirP(new String());
	for(Monom monomCurent1:this.getM()) {//parcurgem lista din polinomul curent
		for(Monom monomCurent2:polinom2.getM()) {//parcurgem lista din al doilea polinom
			Monom monomRez = monomCurent1.inmultireMon(monomCurent2);//luam fiecare monom si le inmultim
			polinom3.getM().add(monomRez);//adaugam monomul rezultat in lista de monoame a polinomului rezultat
		}
		polinom3.analizaSoartarePolinom();
	}
	return polinom3;
}
public Polinom deriveaza() {
	Polinom polinom3 = new Polinom();
	polinom3.setSirP(new String());
	
	for(Monom monomCurent:this.getM()) {
		monomCurent.derivareMonom(); //derivam monom cu monom si le adaugam in polinomul rezultat
		polinom3.getM().add(new Monom(monomCurent.getPutere(),monomCurent.getCoeficient()));
	}
	polinom3.removeCoefZero();
	return polinom3;
}
public Polinom integreaza() {
	Polinom polinom3 = new Polinom();
	polinom3.setSirP(new String());
	for(Monom monomCurent:this.getM()) {
		monomCurent.integrareMonom();
		polinom3.getM().add(new Monom(monomCurent.getPutere(),monomCurent.getCoeficient()));
	}
	polinom3.removeCoefZero();
	return polinom3;
}
private int getIndexPutere(int putere) {   //cauta pozitia in lista de monoame a polinomului in care se afla o anumita putere
	for(int i=0;i<getM().size();i++) {
		Monom monomActual = getM().get(i);
			if(putere == monomActual.getPutere()) 
			return i;
		}
	return -2;
}
private void removeCoefZero() {         //daca un monom din array-ul de monoame are coeficientul 0 atunci acesta este eliminat din lista
	for(int i=0;i<getM().size();i++) {
		if(getM().get(i).getCoeficient() == 0) 
			getM().remove(i);
	}
}
public String getSirP() {
	return sirP;
}
public void setSirP(String sirP) {
	this.sirP = sirP;
}
public ArrayList<Monom> getM() {
	return m;
}
public void setM(ArrayList<Monom> m) {
	this.m = m;
}
}
 
 